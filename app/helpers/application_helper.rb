module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Movies"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def manager_page
    @user = User.new
    @users = User.all.page(params[:page]).per(30).order('created_at DESC')
    @user_movies_list = Movie.all.page(params[:page]).per(20).order('movie_path ASC')
    @movies_list = Movie.all.order('title ASC')
  end


end
