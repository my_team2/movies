class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  load_and_authorize_resource param_method: :user_params
  include ApplicationHelper


  def show
    @user = User.find(params[:id])
  end


  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)
    if current_user.presence && @user.save
      flash[:success] = "#{@user.name} #{@user.email} Added"
     redirect_to manager_panel_path
    elsif @user.save
      log_in @user
      flash[:success] = "Welcome to the ebay-movies.club"
      redirect_to movies_path
    elsif
      if current_user.role == 'admin'
        manager_page
        flash[:danger] = "You not created NEW USER"
        redirect_to manager_panel_path
      else
        render 'new'
      end
    end
  end


  def edit
    @user = User.find(params[:id])
  end


  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Dear #{@user.name} (#{@user.name}) Your Profile Updated"
      redirect_to movies_path
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id]).destroy
    flash[:success] = "User #{@user.name} was DELETED"
    redirect_to manager_panel_path
  end

  def ban
    @user = User.find(params[:id])
    if @user.role != 'admin' && @user.update(role: 'ban')
      flash[:success] = "#{@user.name} BANED"
    else
      flash[:danger] = "User NOT BANED"
    end
    redirect_to manager_panel_path
  end

  def unban
    @user = User.find(params[:id])
    @user.update(role: '')
    flash[:success] = "#{@user.name} UNBANED"
    redirect_to manager_panel_path
  end


  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :role)
  end



  # Before filters

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end


end
