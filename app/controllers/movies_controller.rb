class MoviesController < ApplicationController
  load_and_authorize_resource
  include ApplicationHelper


  def home
    @movies = Movie.all.order('created_at DESC').page(params[:page]).per(20)
    @movies_genre = Movie.all.select(:genre).uniq.order('genre ASC')
    @date_relist = Movie.all.select(:date_relist).uniq.order('date_relist DESC')
    @rating = (Movie.all.select(:rating).uniq).order('rating DESC')
    if params[:search]
      search
    end
  end




  def show
    @movies = current_user.movies.order('created_at DESC').page(params[:page]).per(20)
    if current_user.present? && current_user.role != 'ban'
      get_manager_page
    else
      flash[:danger] = "Sorry you are not registered or was banned"
      redirect_to root_path
    end

  end


  def create
    @movie = Movie.find(params[:Movies])
    @user = User.find_by_id(params[:id])
    @movie_status = @user.movies.find_by_id(@movie.id)
    if @movie_status == nil
      flash[:success] = "Movie '#{@movie.title.upcase}' to '#{@user.email.upcase}' Added"
      @user.movies << @movie
    else
      flash[:danger] = "This is Movie (#{@movie.title.upcase}) has already been added to this user (#{@user.email.upcase})"
    end
   redirect_to manager_panel_path
  end

  def destroy
    movie = Movie.find(params[:id]).destroy
    flash[:success] = "Movie #{movie.title} was DELETED"
    get_manager_page
    render action: 'manager'
  end



  def update
    @user_movies_list = Movie.find(params[:id])
    if @user_movies_list.update_attributes(params.require(:movie).permit(:title, :img, :genre, :date_relist,
                                                                         :ebay_link, :runtime, :rating, :desc, :movie_path, :trailer))
      flash[:success] = "Profile updated"
    else
      flash[:danger] = "Profile not updated"
    end
  end

  def manager
   @tab = 'movie'
   get_manager_page
  end

  def start_parsing
    ContentPopcorn.perform_async
  end

  private

  def search
    @movies = Movie.search(params[:search]).order('updated_at DESC').page(params[:page]).per(20)
    @movies = @movies.genre(params[:genre]).order('updated_at DESC').page(params[:page]).per(20)
    @movies = @movies.date_relist(params[:date_relist]).order('updated_at DESC').page(params[:page]).per(20)
    @movies = @movies.rating(params[:rating]).order('updated_at DESC').page(params[:page]).per(20)
  end

  def get_manager_page
    manager_page
    if params[:search]
      search
      @user_movies_list = @movies
    end
  end
end
