class StaticPagesController < ApplicationController

  def new
    @contact = ContactForm.new
  end

  def create
    @contact = ContactForm(contact_params)
  end

  def contact_params
    params.require(ContactForm).permit(:name, :email, :text)
  end
end
