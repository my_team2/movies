class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include SessionsHelper
  rescue_from CanCan::AccessDenied do |exception|
    if request.xhr?
      render :json => ['You are not authorised to do that.'], :status => :unprocessable_entity
    else
      redirect_to '/', :alert => exception.message
    end
  end

end
