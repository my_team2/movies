class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    alias_action :create, :read, :update, :new, :edit, to: :user_action_access
    alias_action :home, :show, to: :movie_action_access
    alias_action :home, :show, to: :guest_movie
    alias_action :home, :show,:new, :create, to: :guest_user
    if user.present?
      if user.role == 'admin'
        can :manage, :all
      else
        can :movie_action_access, Movie
        can :user_action_access, User
      end
      if user.role =='ban'
        cannot :read, :manage, :all
      end
    else
      can :guest_movie, Movie
      can :guest_user, User
      #   # All registered users
    #   can {registered user-y permissions}
    #   # Admins
    #   if user.is_admin?
    #     can :manage, :all
    #   end
    # if user.present?
    #   can :manage, :all if user.role == "admin"

    end


    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
