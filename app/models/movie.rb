class Movie < ApplicationRecord
  validates :title, uniqueness: { case_sensitive: false }
  has_and_belongs_to_many :users

    scope :search, -> (search) {where("title LIKE ?", "%#{search}%")}
    scope :genre, -> (genre) {where("genre LIKE ?", "%#{genre}%")}
    scope :date_relist, -> (date_relist) {where("date_relist LIKE ?", "%#{date_relist}%")}
    scope :rating, -> (rating) {where("rating LIKE ?", "%#{rating}%")}
end


