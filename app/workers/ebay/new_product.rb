require 'rubygems'
require 'httparty'
require 'net/http'
EBAY_CONFIG = YAML::load(File.open("config/ebay.yml"))['development']

module Ebay
class NewProduct
  require 'rails/all'
  require 'sidetiq'
  require 'sidekiq'
  include HTTParty
  include Sidekiq::Worker
  include Sidetiq::Schedulable

      # recurrence { minutely(1) }

  def getEbayOfficialTime
    # uri = URI('https://svcs.ebay.com/services/search/FindingService/v1?SECURITY-APPNAME=AndriyKo-testblab-PRD-59ef347fe-03e5690b&OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&keywords=apple&paginationInput.entriesPerPage=6&GLOBAL-ID=EBAY-US&siteid=0')
    # page = Net::HTTP.get(uri)
    # puts page
    #
    #
    # format :xml
    #
    # {"X-EBAY-API-COMPATIBILITY-LEVEL"=>"433", "X-EBAY-API-DEV-NAME"=>"35de17f0-b043-40e4-9c0a-b1cf3664f82a", "X-EBAY-API-APP-NAME"=>"AndriyKo-testblab-SBX-82f5e2e0d-f64d9f7b", "X-EBAY-API-CERT-NAME"=>"SBX-2f5e2e0d15ee-f2a8-4cc3-be7b-5de9", "X-EBAY-API-SITEID"=>"0", "Content-Type"=>"text/xml"}
    #
    # requestXml = "<?xml version='1.0' encoding='utf-8'?>
    #               <GeteBayOfficialTimeRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">
    #                 <RequesterCredentials>
    #                   <eBayAuthToken></eBayAuthToken>
    #                 </RequesterCredentials>
    #               </GeteBayOfficialTimeRequest>".xml
    #
    # response = post(api_url, :body => requestXml)
    # raise "Bad Response | #{response.inspect}" if response.parsed_response['GeteBayOfficialTimeResponse']['Ack'] != 'Success'
    # response.parsed_response['GeteBayOfficialTimeResponse']['Timestamp']
    # puts "AAAAAAAAA"
    # return response.parsed_response['GeteBayOfficialTimeResponse']['Timestamp']
  end


  def self.ebay_headers
    {"X-EBAY-API-COMPATIBILITY-LEVEL" => "433",
     "X-EBAY-API-DEV-NAME" => EBAY_CONFIG['dev_id'],
     "X-EBAY-API-APP-NAME" => EBAY_CONFIG['app_id'],
     "X-EBAY-API-CERT-NAME" => EBAY_CONFIG['cert_id'],
     "X-EBAY-API-SITEID" => "0",
     "Content-Type" => "text/xml"}
  end
  puts ebay_headers
  def self.auth_token
    EBAY_CONFIG['auth_token']
  end

  def self.api_url
    EBAY_CONFIG['uri']
  end






  def perform
    getEbayOfficialTime
  end
  end

end
