function moveon() {
        document.getElementById("user_password").onchange = validatePassword;
        document.getElementById("user_password_confirmation").onchange = validatePassword;
}
setTimeout(moveon, 100);

function validatePassword(){
    var pass2=document.getElementById("user_password_confirmation").value;
    var pass1=document.getElementById("user_password").value;
    if(pass1!=pass2)
        document.getElementById("user_password_confirmation").setCustomValidity("Passwords Don't Match");
    else
        document.getElementById("user_password_confirmation").setCustomValidity('');
//empty string means no validation error
}