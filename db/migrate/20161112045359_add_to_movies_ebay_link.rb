class AddToMoviesEbayLink < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :ebay_link, :string
  end
end
