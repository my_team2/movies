class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :img
      t.string :genre
      t.integer :date_relist
      t.string :runtime
      t.integer :rating
      t.text :desc
      t.string :movie_path

      t.timestamps
    end
  end
end
