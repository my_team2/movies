class AddColumnEbayIdToMovies < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :ebay_id, :integer
  end
end
