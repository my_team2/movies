class AddStatusDownloadToMovies < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :downloaded, :boolean, default: false
    add_column :movies, :converted, :boolean, default: false
  end
end
