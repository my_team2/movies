class AddCreatedAtToMoviesUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :movies_users, :created_at, :datetime
  end
end
