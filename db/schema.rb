# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170129054522) do

  create_table "contact_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.text     "text",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "movies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "img"
    t.string   "genre"
    t.integer  "date_relist"
    t.string   "runtime"
    t.integer  "rating"
    t.text     "desc",        limit: 65535
    t.string   "movie_path"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "trailer"
    t.integer  "ebay_id"
    t.string   "ebay_link"
    t.boolean  "downloaded",                default: false
    t.boolean  "converted",                 default: false
  end

  create_table "movies_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "movie_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.index ["movie_id"], name: "index_movies_users_on_movie_id", using: :btree
    t.index ["user_id"], name: "index_movies_users_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "role"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

end
